table! {
    use diesel::sql_types::BigInt;
    use crate::perms::PermissionMapping;

    privileged_users (id) {
        id -> BigInt,
        max_perm -> PermissionMapping,
    }
}
