use serenity::model::id::GuildId;
use serenity::model::voice::VoiceState;
use super::*;

pub fn handle_vc_text_channel(gid: GuildId, state: VoiceState) {
    if *gid.as_u64() != MAPLE_KIDDOS_ID {
        return;
    }

    let mut member: Member = gid.member(state.user_id).unwrap();

    // channel_id is Some if they're in a voice channel and None otherwise
    if let Some(ch_id) = state.channel_id {
        // If they're not joining an authorized channel make sure to remove the role
//        if *ch_id.as_u64() != GENERAL_VC_ID && *ch_id.as_u64() != MUSIC_VC_ID {
//            if member.roles.iter().any(|role| *role.as_u64() == VC_ROLE_ID) {
//                member.remove_role(RoleId(VC_ROLE_ID)).unwrap();
//            }
//            return;
//        }

//        if *ch_id.as_u64() != GAMING_VC1_ID && *ch_id.as_u64() != GAMING_VC2_ID {
//            if member.roles.iter().any(|role| *role.as_u64() == GAMER_ROLE_ID) {
//                member.remove_role(RoleId(GAMER_ROLE_ID)).unwrap();
//            }
//            return;
//        }

        match *ch_id.as_u64() {
            GENERAL_VC_ID | MUSIC_VC_ID => {
                if !member.roles.iter().any(|role| *role.as_u64() == VC_ROLE_ID) {
                    println!("Trying to add the vc role");
                    member.add_role(RoleId(VC_ROLE_ID)).unwrap();
                }
                if member.roles.iter().any(|role| *role.as_u64() == GAMER_ROLE_ID) {
                    member.remove_role(RoleId(GAMER_ROLE_ID)).unwrap();
                }
            }
            MAPLEKRAFT_VC_ID | GAMING_VC_ID => {
                if !member.roles.iter().any(|role| *role.as_u64() == GAMER_ROLE_ID) {
                    println!("Trying to add gamer role");
                    member.add_role(RoleId(GAMER_ROLE_ID)).unwrap();
                }
                if member.roles.iter().any(|role| *role.as_u64() == VC_ROLE_ID) {
                    member.remove_role(RoleId(VC_ROLE_ID)).unwrap();
                }
            }
            _ => {}
        }
    } else {
        // If they're leaving vc and have the role, remove it
        if member.roles.iter().any(|role| *role.as_u64() == VC_ROLE_ID) {
            println!("trying to remove the vc role");
            member.remove_role(RoleId(VC_ROLE_ID)).unwrap();
        }
        if member.roles.iter().any(|role| *role.as_u64() == GAMER_ROLE_ID) {
            println!("Trying to remove gamer role");
            member.remove_role(RoleId(GAMER_ROLE_ID)).unwrap();
        }
    }
}