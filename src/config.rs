use serde_derive::*;
use std::fs::File;
use std::path::Path;
use std::io::Read;
use typemap::Key;

#[derive(Deserialize, Debug)]
pub struct Config {
    pub warn_log_channel_id: u64
}

pub struct ConfigKey;

impl Key for ConfigKey {
    type Value = Config;
}

pub fn read_config_file() -> Option<Config> {
    let path = Path::new("maplebot.toml");
    if path.exists() {
        let mut file = File::open(path).ok()?;
        let mut vec = Vec::new();
        file.read_to_end(&mut vec);

        toml::from_slice::<Config>(&vec[..]).ok()
    }else {
        None
    }
}
