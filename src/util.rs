use std::time::{Instant, Duration};

pub struct TtlVec<T> {
    storage: Vec<InternalValue<T>>,
}

impl<T> TtlVec<T> {
    pub fn new() -> TtlVec<T> {
        TtlVec { storage: Vec::new() }
    }

    pub fn with_capacity(cap: usize) -> TtlVec<T> {
        TtlVec { storage: Vec::with_capacity(cap) }
    }

    pub fn push(&mut self, value: T, expiration_duration: Duration) {
        if self.storage.len() >= self.storage.capacity() {
            self.remove_oldest();
        }

        self.storage.push(InternalValue::new(value, expiration_duration))
    }

    pub fn get(&self, idx: usize) -> Option<&T> {
        self.storage.get(idx).and_then(|value| if value.is_expired() { None } else { Some(&value.value) })
    }

    pub fn get_mut(&mut self, idx: usize) -> Option<&mut T> {
        self.storage.get_mut(idx).and_then(|value| if value.is_expired() { None } else { Some(&mut value.value) })
    }

    pub fn clear(&mut self) {
        self.storage.clear();
    }

    fn remove_oldest(&mut self) {
        if let Some(idx) = self.storage.iter().enumerate().min_by(|(_, value), (_, value2)| value.expiration.cmp(&value2.expiration))
            .map(|(idx, _)| idx) {

            self.storage.remove(idx);
        }
    }
}

pub struct InternalValue<T> {
    value: T,
    expiration: Instant,
}

impl<T> InternalValue<T> {
    pub fn new(value: T, expiration: Duration) -> InternalValue<T> {
        InternalValue {
            value,
            expiration: Instant::now() + expiration
        }
    }

    pub fn value(&self) -> &T {
        &self.value
    }

    pub fn value_mut(&mut self) -> &mut T {
        &mut self.value
    }

    pub fn is_expired(&self) -> bool {
        Instant::now() > self.expiration
    }
}