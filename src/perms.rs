use serenity::model::id::UserId;
use crate::schema::privileged_users;
use diesel::prelude::*;
use std::str::FromStr;
use failure::bail;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, DbEnum)]
pub enum Permission {
    Warn,
    Mute,
    All,
}

impl FromStr for Permission {
    type Err = failure::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "warn" => Ok(Permission::Warn),
            "mute" => Ok(Permission::Mute),
            "all" => Ok(Permission::All),
            _ => bail!("Invalid permission specifier {}", s)
        }
    }
}

#[derive(Debug, Queryable, Insertable)]
#[table_name = "privileged_users"]
pub struct PrivilegedUser {
    pub id: i64,
    pub max_perm: Permission,
}

impl PrivilegedUser {
    pub fn new(id: i64, max_perm: Permission) -> PrivilegedUser {
        PrivilegedUser {
            id,
            max_perm
        }
    }
}