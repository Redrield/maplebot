// role id for general people
pub const GENERAL_PEOPLE_ROLE_ID: u64 = 443449748041629706;
// maple kiddos server id
pub const MAPLE_KIDDOS_ID: u64 = 438034919734640651;
// General text channel id
pub const GENERAL_CHANNEL_ID: u64 = 438034920452128769;
// General vc channel id
pub const GENERAL_VC_ID: u64 = 438034920452128771;
pub const MUSIC_VC_ID: u64 = 438049248370491403;
pub const VC_ROLE_ID: u64 = 567515164392292352;

// Gaming specific vc stuff
pub const GAMER_ROLE_ID: u64 = 511353149609476112;
pub const GAMING_VC_ID: u64 = 443547443791921154;
pub const MAPLEKRAFT_VC_ID: u64 = 576221360578232328;

// These are the constants for lambda in the test server
pub const TEST_ID: u64 = 463840606175756288;
pub const TEST_ROLE_ID: u64 = 564641766129532940;
pub const TEST_CHANNEL_ID: u64 = 463840606175756290;
pub const TEST_VC_ID: u64 = 463840606175756292;
pub const TEST_VC_ROLE_ID: u64 = 567508262157549568;

pub const ME: u64 = 159468393051848704;