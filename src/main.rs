#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_derive_enum;

use serenity::prelude::*;
use serenity::model::id::{GuildId, RoleId, ChannelId, UserId};
use serenity::model::guild::{Member, AuditLogs};
use std::collections::HashMap;
use std::env;
use serenity::model::channel::{GuildChannel, Message};
use serenity::utils::MessageBuilder;
use serenity::model::voice::VoiceState;
use serenity::model::user::User;
use serenity::command;
use typemap::Key;
use diesel::sqlite::SqliteConnection;
use diesel::prelude::*;

mod joining;
mod voice;
mod commands;
mod util;
mod perms;
mod schema;
mod config;

use config::*;

mod consts;
pub use self::consts::*;
use serenity::framework::StandardFramework;
use crate::util::TtlVec;
use std::time::Duration;
use crate::perms::*;
use diesel::query_dsl::RunQueryDsl;

pub struct MapleBot {
    db: SqliteConnection,
    privileged_users: Vec<PrivilegedUser>,
}

unsafe impl Sync for MapleBot {}

impl MapleBot {
    fn new() -> MapleBot {
        use self::schema::privileged_users;

        let db = establish_connection();
        let privileged_users = privileged_users::table.load::<PrivilegedUser>(&db)
            .expect("Failed to load users");

        MapleBot {
            db,
            privileged_users
        }
    }

    pub fn update_user(&mut self, new_data: PrivilegedUser) {
        use schema::privileged_users;

        if let Some(idx) = self.privileged_users.iter().enumerate().find(|(_, user)| user.id == new_data.id).map(|(id, _)| id) {
            self.privileged_users.remove(idx);
        }
        diesel::insert_into(privileged_users::table)
            .values(&new_data)
            .execute(&self.db)
            .expect("Failed to update database");
        self.privileged_users.push(new_data);

    }

    pub fn delete_user(&mut self, user_id: i64)  {
        if let Some(idx) = self.privileged_users.iter().enumerate().find(|(_, user)| user.id == user_id).map(|(id, _)| id)  {
            self.privileged_users.remove(idx);

            use schema::privileged_users::dsl::*;
            diesel::delete(privileged_users.filter(id.eq(user_id)))
                .execute(&self.db)
                .expect("Error removing user");

        }
    }

    pub fn check_perms(&self, id: UserId, req: Permission) -> bool {
        match self.privileged_users.iter().find(|user| user.id as u64 == id.0) {
            Some(user) => {
                println!("Got user {:?}", user);
                println!("Privilege level predicate {}", user.max_perm >= req);
                user.max_perm >= req
            },
            None => {
                println!("Couldn't find user");
                false
            }
        }
    }
}

fn establish_connection() -> SqliteConnection {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    SqliteConnection::establish(&database_url)
        .expect(&format!("Couldn't find database file for {}", database_url))
}

struct MapleBotKey;

impl Key for MapleBotKey {
    type Value = MapleBot;
}

pub struct Handler;

impl EventHandler for Handler {
    fn guild_member_addition(&self, _ctx: Context, gid: GuildId, member: Member) {
        joining::handle_new_member(gid, member);
    }

    fn voice_state_update(&self, _ctx: Context, gid: Option<GuildId>, state: VoiceState) {
        if let Some(id) = gid {
            voice::handle_vc_text_channel(id, state);
        }
    }
}

fn main() {
    let token = env::var("DISCORD_TOKEN").expect("Unable to get bot token");
    let mut client = Client::new(&token, Handler)
        .expect("Unable to create client");

    client.data.lock().insert::<MapleBotKey>(MapleBot::new());

    if let Some(config) = read_config_file() {
        println!("Got config {:?}", &config);
        client.data.lock().insert::<ConfigKey>(config);
    }

    client.with_framework(
        StandardFramework::new()
            .configure(|c| c.allow_whitespace(true)
                .prefix("m!"))
            .command("ping", |c| c.cmd(ping))
            .command("poll", |c| c.cmd(poll))
            .command("warn", |c| c.cmd(warn))
            .command("manage", |c| c.cmd(manage))
    );

    client.start().unwrap()
}

command!(ping(_ctx, msg, _args) {
    msg.channel_id.say("Pong!").unwrap();
});

command!(poll(ctx, msg, args) {
    commands::poll(ctx, msg, args);
});

command!(warn(ctx, msg, args) {
    commands::warn(ctx, msg, args);
});

command!(manage(ctx, msg, args) {
    commands::manage(ctx, msg, args);
});