use super::*;

/// Function to handle giving the role to people if they are new to the server, and notifying them to change their nick
pub fn handle_new_member(gid: GuildId, mut member: Member) {
    if *gid.as_u64() == MAPLE_KIDDOS_ID && !member.user_id().to_user().expect("Unable to get user").bot {
        println!("Got a new member");
        if !member.roles.iter().any(|role| *role.as_u64() == GENERAL_PEOPLE_ROLE_ID) {
            println!("Finna add role");
            member.add_role(RoleId(GENERAL_PEOPLE_ROLE_ID)).expect("Failed to add role.");

            let response = MessageBuilder::new()
                .push("Welcome ")
                .mention(&member)
                .push(" to Maple Kiddos! Be sure to read the rules, and to add your team number to your nickname")
                .build();
            let channels: HashMap<ChannelId, GuildChannel> = gid.channels().unwrap();
            channels[&ChannelId(GENERAL_CHANNEL_ID)].say(&response).unwrap();
        }
    }
}
