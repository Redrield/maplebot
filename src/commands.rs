use std::str::FromStr;
use serenity::client::Context;
use serenity::model::channel::Message;
use serenity::framework::standard::Args;
use crate::MapleBotKey;
use crate::perms::{Permission, PrivilegedUser};
use crate::config::ConfigKey;
use serenity::model::id::ChannelId;
use serenity::model::prelude::*;

pub fn poll(_ctx: &mut Context, msg: &Message, _args: Args) {
    msg.react("👍").unwrap();
    msg.react("👎").unwrap();
}

pub fn warn(ctx: &mut Context, msg: &Message, mut args: Args) {
    let data = &ctx.data.lock();
    let bot = data.get::<MapleBotKey>().unwrap();

    if bot.check_perms(msg.author.id, Permission::Warn) {
        if msg.mentions.len() != 1 {
            msg.reply("Please use the command as follows: `m!warn <user> <reason>`").unwrap();
            return;
        }
        let user = &msg.mentions[0];
        args.next();
        msg.channel_id.say(format!("Warning {} for {}", user.mention(), args.rest())).unwrap();

        if let Some(config) = data.get::<ConfigKey>() {
            println!("Trying to log message");
            let channel = ChannelId(config.warn_log_channel_id);
            channel.send_message(|m| {
                m.embed(|e| {
                    e
                        .title("User Warned")
                        .field("Staff Member", msg.author.mention(), false)
                        .field("Channel", msg.channel_id.name().unwrap(), true)
                        .field("User", user.mention(), true)
                        .field("Reason", args.rest(), false)
                })
            }).unwrap();
        }
        //TODO: Add a log of this event
    } else {
        msg.reply("You don't have permission to perform this action!");
    }
}

pub fn manage(ctx: &mut Context, msg: &Message, mut args: Args) {
    let mut data = ctx.data.lock();
    let bot = data.get_mut::<MapleBotKey>().unwrap();

    if bot.check_perms(msg.author.id, Permission::All) {
        if msg.mentions.len() != 1 {
            msg.reply("Please use the command as follows: `m!manage <user> <add|remove> <warn|mute|all>`").unwrap();
            return;
        }
        let user = &msg.mentions[0];

        args.next();
        if args.remaining() < 1 {
            msg.reply("Please use the command as follows: `m!manage <user> <add|clear> [warn|mute|all]`").unwrap();
            return;
        }

        match args.single::<String>().unwrap().as_str() {
            "add" => {
                if args.remaining() < 1 {
                    msg.reply("Please use the command as follows: `m!manage <user> <add|clear> [warn|mute|all]`").unwrap();
                    return;
                }
                let perm_str = args.single::<String>().unwrap();
                let perm = Permission::from_str(&perm_str).unwrap();
                bot.update_user(PrivilegedUser::new(user.id.0 as i64, perm));
                msg.reply(&format!("Added permission {} to user {}", perm_str, user.tag())).unwrap();
            }
            "clear" => {
                bot.delete_user(user.id.0 as i64);
                msg.reply(&format!("Cleared permissions for user {}", user.tag())).unwrap();
            }
            _ => {
                msg.reply("Please use the command as follows: `m!warn <user> <add|remove> <warn|mute|all>`").unwrap();
            }
        }
    } else {
        msg.reply("You don't have permission to perform this action!").unwrap();
    }
}