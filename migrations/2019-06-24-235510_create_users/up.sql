-- Your SQL goes here
CREATE TABLE privileged_users(
    id BIG INTEGER NOT NULL PRIMARY KEY,
    max_perm TEXT CHECK(max_perm IN ('all', 'mute', 'warn')) NOT NULL
);

INSERT INTO privileged_users VALUES (159468393051848704, 'all');